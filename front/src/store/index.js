import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  plugins: [
    createPersistedState()
  ],
  state: {
    shop:null,
    discount:null,
    cost_ship:0,
    address_ship:null,
    cart:[],
    user:{
      isLogin:false
    }
  },
  mutations: {
    SET_ITEM_TO_CART(state,item){
      item.qty_buy = 1
      state.cart.push(item)
    },
    UPDATE_ITEM_QRT(state,items){
      state.cart = state.cart.map((p)=>{
        if(p.id==items.id) {
          items.qty_buy+=1
          return items
        }
        return p
      })
    },
    SET_NEW_QTY(state,items){
      state.cart = state.cart.map((p)=>{
        if(p.id==items.id) {
          return items
        }
        return p
      })
    },
    SET_DISCOUNT(state,items){
      state.discount = items
    },
    SET_COST_SHIP(state,items){
      state.cost_ship=items
    },
    SET_ADRRESS_SHIP(state,items){
      state.address_ship = items
    },
    DELETE_ITEM_AT_CART(state,items){
      let products = state.cart.filter((item)=>item.id!=items.id)
      state.cart=products
    },

    SET_USER_LOGIN(state,item){
      console.log("store")
      console.log(item)
      state.user = item
      state.user.isLogin = true
    },
    SET_USER_LOGOUT(state){
      state.user = {}
      state.user.isLogin = false
      state.cart = []
    },
    SET_ALL_STATE(state){
      state.discount=null,
      state.cost_ship=0,
      state.address_ship=null,
      state.cart=[]
    }
  },
  actions: {
    addItemToCart({commit},items){
      if(this.state.cart.length>0){
        console.log(this.state.cart[0].StoreBranchId)
        console.log(items.StoreBranchId)
        if(this.state.cart[0].StoreBranchId == items.StoreBranchId){
          let haveProduct = this.state.cart.find((item)=>item.id==items.id)
          if(haveProduct){
            commit("UPDATE_ITEM_QRT",haveProduct)
          }else{
            commit("SET_ITEM_TO_CART",items)
          }
          
        }else{
          alert("ไม่สามารถเพิ่มสินค้าลงตะกร้าได้ เนื่องจากมีสินค้าจากสาขาอื่นในตระกร้าแล้ว")
        }
      }else{
        let haveProduct = this.state.cart.find((item)=>item.id==items.id)

        if(haveProduct){
          commit("UPDATE_ITEM_QRT",haveProduct)
        }else{
          commit("SET_ITEM_TO_CART",items)
        }
      }

      
      
    },
    updateQrtProduct({commit},items){
      commit("SET_NEW_QTY",items)
    },

    deleteProduct({commit},item){
      commit("DELETE_ITEM_AT_CART",item)

    },
    addDiscount({commit},items){
      commit("SET_DISCOUNT",items)
    },
    addCostShip({commit},items){
      commit("SET_COST_SHIP",items)
    }, 
    addAddressShip({commit},items){
      commit("SET_ADRRESS_SHIP",items)
    },  
    addUserLogin({commit},items){
      commit("SET_USER_LOGIN",items)
    },

    UserLogout({commit}){
      commit("SET_USER_LOGOUT")
    },
    clearAllState({commit}){
      commit("SET_ALL_STATE")
    }

  },
  getters:{
    cart:(state)=>state.cart,
    user:(state)=>state.user,
    sumProductTotal:(state)=>{
      return state.cart.reduce((a, b) => a + b.qty_buy * b.price, 0);
    },
    sumDiscount:(state)=>{
      if(state.discount){
        return state.discount.price
      }else{
        return 0
      }
      
    },
    sumCostShip:state => state.cost_ship
  },
  modules: {
  }
})

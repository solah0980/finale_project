import Vue from 'vue'
import VueRouter from 'vue-router'
//ส่วนของซื้อของออนไลน์
import Shop from '../views/Shop.vue'
import ShowProduct from '../views/shopping/ShowProduct.vue'
import Cart from '../views/Cart.vue'
import SelectAddress from '../views/shopping/SelectAddress.vue'
import Payment from '../views/shopping/Payment.vue'
import Success from '../views/shopping/ShopSuccess.vue'
import Home from '../views/Home.vue'
import Coupon from '../views/Coupon.vue'

//ส่วนของ backend พนักงาน
import EmployeeCreate from '../views/backend/employeeManage/Create.vue'
import EmployeeShow from '../views/backend/employeeManage/Show.vue'
import EmployeeEdit from '../views/backend/employeeManage/Edit.vue'
import AdminLogin from '../views/backend/login/AdminLogin.vue'
import EmployeeIndex from '../views/backend/employeeProfile/Index.vue'

//ส่วนของ backend จัดการค่าจ้าง
import SalaryCreate from '../views/backend/salaryManage/Create.vue'
import SalaryPage from '../views/backend/salaryManage/PageShow.vue'
import SalaryEdit from '../views/backend/salaryManage/Edit.vue'
import ShowEmployee from '../views/backend/salaryManage/ShowEmployee.vue'

//ส่วนของ backend จัดการร้าน
import ShopCreate from '../views/backend/shopManage/Create.vue'
import ShopShow from '../views/backend/shopManage/Show.vue'
import ShopRead from '../views/backend/shopManage/Read.vue'

//ส่วนของ backend จัดการประเภทสินค้า
import TypeProductCreate from '../views/backend/type_productManage/Create.vue'
import TypeProductEdit from '../views/backend/type_productManage/Edit.vue'
import TypeProductShow from '../views/backend/type_productManage/Show.vue'


//ส่วนของ backend จัดการสินค้า
import ProductCreate from '../views/backend/productManage/Create.vue'
import ProductEdit from '../views/backend/productManage/Edit.vue'
import ProductShow from '../views/backend/productManage/Show.vue'

//ส่วนของ backend จัดการสต็อกสินค้า
import StockProductSelectOrder from '../views/backend/stock_productManage/SelectOrder.vue'
import StockProductCreate from '../views/backend/stock_productManage/Create.vue'
import StockProductShow from '../views/backend/stock_productManage/Show.vue'
import StockProductEdit from '../views/backend/stock_productManage/Edit.vue'

//ส่วนของ backend จัดการสินค้าเสีย
import WeastProductShow from '../views/backend/weast_productManage/Show.vue'

//ส่วนของ backend จัดการสั่งซื้อสินค้า
import OrderProductCreate from '../views/backend/order_productManage/Create.vue'
import OrderProductShow from '../views/backend/order_productManage/Show.vue'
import OrderProductIndex from '../views/backend/order_productManage/Index.vue'
import OrderProductOwnerCheck from '../views/backend/order_productManage/OwnerCheck.vue'
import OrderProductEdit from '../views/backend/order_productManage/Edit.vue'

//ส่วนของ backend จัดการสั่งซื้อสินค้า
import SupplierCreate from '../views/backend/supplierManage/Create.vue'
import SupplierShow from '../views/backend/supplierManage/Show.vue'
import SupplierEdit from '../views/backend/supplierManage/Edit.vue'

//ส่วนของ backend ลูกค้า
import CustomerShow from '../views/backend/customerManage/Show.vue'
import CustomerCheck from '../views/backend/customerManage/Edit.vue'

//ส่วนของลูกค้า
import Register from '../views/Register.vue'
import Login from '../views/Login.vue'
import AddressShipment from '../views/customer/Address.vue'
import AddAddress from '../views/customer/addAddress.vue'
import CustomerEdit from '../views/customer/PersonalEdit.vue'
import PasswordEdit from '../views/customer/PasswordEdit.vue'
import Purchase from '../views/customer/Purchase.vue'
import PurchaseById from '../views/customer/PurchaseById.vue'

//ส่วนของ POS
import PosCreate from '../views/backend/pos/Create.vue'
import PosShow from '../views/backend/pos/Show.vue'
import PosEdit from '../views/backend/pos/Edit.vue'

//ส่วนของ CRM
import CrmEmail from '../views/backend/crm/CrmEmail.vue'
import CrmPromotionCreate from '../views/backend/crm/CreatePromotion.vue'
import CrmPromotionShow from '../views/backend/crm/ShowPromotion.vue'

import Chart from '../components/backend/chartPos.vue'

//ส่วนของ จัดการออร์เดอร์ลูกค้า
import OrderCustomerShow from '../views/backend/orderCustomer/Show.vue'
import ManageOrderCustomer from '../views/backend/orderCustomer/ManageOrder.vue'

//ส่วนของ จัดการการเงิน
import FinanceShow from '../views/backend/finance/Show.vue'
import SalesFinanceShow from '../views/backend/finance/Sales.vue'
import IncomeFinanceShow from '../views/backend/finance/IncomeStatement'
Vue.use(VueRouter)

const routes = [
  //ส่วนของซื้อของออนไลน์
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/shoping',
    name: 'Shop',
    component: Shop
  },
  {
    path: '/coupon',
    name: 'Coupon',
    component: Coupon
  },
  {
    path: '/shoping/:id',
    name: 'ShowProduct',
    component: ShowProduct
  },
  {
    path: '/cart',
    name: 'Cart',
    component: Cart
  },
  {
    path: '/cart/select_address',
    name: 'SelectAddress',
    component: SelectAddress
  },
  {
    path: '/cart/payment',
    name: 'Payment',
    component: Payment
  },
  {
    path: '/success',
    name: 'Success',
    component: Success
  },
//---------------------------------------------------------------------//
//ส่วนของ ลูกค้า
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login
  },
  {
    path: '/customer/:id',
    name: 'CustomerEdit',
    component: CustomerEdit
  },
  {
    path: '/purchase',
    name: 'Purchase',
    component: Purchase
  },
  {
    path: '/purchase/order/:id',
    name: 'PurchaseById',
    component: PurchaseById
  },
  {
    path: '/address_shipment/show/:id',
    name: 'AddressShipment',
    component: AddressShipment
  },
  {
    path: '/address_shipment/add',
    name: 'AddAddress',
    component: AddAddress
  },
  {
    path: '/password_edit/:id',
    name: 'PasswordEdit',
    component: PasswordEdit
  },
  //---------------------------------------------------------------------//

  //ส่วนของ backend ลุกค้า
  {
    path: '/admin/customers',
    name: 'CustomerShow',
    component: CustomerShow
  },
  {
    path: '/admin/customer/:id',
    name: 'CustomerCheck',
    component: CustomerCheck
  },



  //ส่วนของ backend พนักงาน และ เจ้าของ
  {
    path: '/admin/employee',
    name: 'EmployeeIndex',
    component: EmployeeIndex
  },
  {
    path: '/admin/employee/create',
    name: 'EmployeeCreate',
    component: EmployeeCreate
  },
  {
    path: '/admin/employees',
    name: 'EmployeeShow',
    component: EmployeeShow
  },
  {
    path: '/admin/employee/edit/:id',
    name: 'EmployeeEdit ',
    component: EmployeeEdit
  },
  {
    path: '/admin/login',
    name: 'AdminLogin',
    component: AdminLogin
  },
 //---------------------------------------------------------------------//
 //ส่วนของ backend จัดการค่าจ้าง
 {
  path: '/admin/salary/read',
  name: 'SalaryPage',
  component: SalaryPage
},
 {
  path: '/admin/salary/create',
  name: 'SalaryPay ',
  component: SalaryCreate
},
{
  path: '/admin/salary/edit/:data',
  name: 'SalaryEdit ',
  component: SalaryEdit
},
{
  path: '/admin/salary/employyee',
  name: 'ShowEmployee ',
  component: ShowEmployee
},
//---------------------------------------------------------------------//
//ส่วนของ backend จัดการร้าน
{
  path: '/admin/store_branch/create',
  name: 'ShopCreate ',
  component: ShopCreate
},
{
  path: '/admin/store_branchs',
  name: 'ShopShow ',
  component: ShopShow
},
{
  path: '/admin/store_branch/read/:id',
  name: 'ShopRead ',
  component: ShopRead
},
//---------------------------------------------------------------------//
//ส่วนของ backend จัดการประเภทสินค้า
{
  path: '/admin/type_product/create',
  name: 'TypeProductCreate ',
  component: TypeProductCreate
},
{
  path: '/admin/type_products',
  name: 'TypeProductShow',
  component: TypeProductShow
},
{
  path: '/admin/type_product/edit/:id',
  name: 'TypeProductEdit',
  component: TypeProductEdit
},
//---------------------------------------------------------------------//
//ส่วนของ backend จัดการสินคา
{
  path: '/admin/product/create',
  name: 'ProductCreate',
  component: ProductCreate
},
{
  path: '/admin/product/edit/:id',
  name: 'ProductEdit',
  component: ProductEdit
},
{
  path: '/admin/products',
  name: 'ProductShow',
  component: ProductShow
},
//---------------------------------------------------------------------//
//ส่วนของ backend จัดการสต็อกสินคา
{
  path: '/admin/stock_product/select_order',
  name: 'StockProductSelectOrder',
  component: StockProductSelectOrder
},
{
  path: '/admin/stock_product/create',
  name: 'StockProductCreate',
  component: StockProductCreate
},
{
  path: '/admin/stock_products',
  name: 'StockProductShow',
  component: StockProductShow
},
{
  path: '/admin/stock_product/edit/:id',
  name: 'StockProductEdit',
  component: StockProductEdit
},

//---------------------------------------------------------------------//
//ส่วนของ backend จัดการสินค้าเสีย
{
  path: '/admin/weast_products',
  name: 'WeastProductShow',
  component: WeastProductShow
},
//---------------------------------------------------------------------//
//ส่วนของ backend จัดการสั่งซื้อสินค้า

{
  path: '/admin/order_product/create',
  name: 'OrderProductCreate',
  component: OrderProductCreate
},
{
  path: '/admin/order_products',
  name: 'OrderProductShow',
  component: OrderProductShow
},
{
  path: '/admin/order_product/:id',
  name: 'OrderProductIndex',
  component: OrderProductIndex
},
{
  path: '/admin/order_product/update_status/:id',
  name: 'OrderProductOwnerCheck',
  component: OrderProductOwnerCheck
},
{
  path: '/admin/order_product/edit/:id',
  name: 'OrderProductEdit',
  component: OrderProductEdit
},
//---------------------------------------------------------------------//
//ส่วนของ backend จัดการซัพพลายเออร์
{
  path: '/admin/supplier/create',
  name: 'SupplierCreate',
  component: SupplierCreate
},
{
  path: '/admin/suppliers',
  name: 'SupplierShow',
  component: SupplierShow
},
{
  path: '/admin/supplier/edit/:id',
  name: 'SupplierEdit',
  component: SupplierEdit
},
//---------------------------------------------------------------------//
//ส่วนของ backend POS
{
  path: '/admin/pos/create',
  name: 'PosCreate',
  component: PosCreate
},
{
  path: '/admin/pos/show',
  name: 'PosShow',
  component: PosShow
},
{
  path: '/admin/pos/:id',
  name: 'PosEdit',
  component: PosEdit
},
//---------------------------------------------------------------------//
//ส่วนของ backend CRM
{
  path: '/admin/crm/email',
  name: 'CrmEmail',
  component: CrmEmail
},
{
  path: '/admin/crm/promotion/create',
  name: 'CrmPromotionCreate',
  component: CrmPromotionCreate
},
{
  path: '/admin/crm/promotion',
  name: 'CrmPromotionShow',
  component: CrmPromotionShow
},

{
  path: '/admin/chart',
  name: 'Chart',
  component: Chart
},
//---------------------------------------------------------------------//
//ส่วนของ backend จัดการออร์เดอร์ลูกค้า
{
  path: '/admin/order_customers',
  name: 'OrderCustomerShow',
  component: OrderCustomerShow
},
{
  path: '/admin/order_customer/show',
  name: 'ManageOrderCustomer',
  component: ManageOrderCustomer
},
//---------------------------------------------------------------------//
//ส่วนของ backend จัดการการเงิน

{
  path: '/admin/finance',
  name: 'FinanceShow',
  component: FinanceShow
},
{
  path: '/admin/finance/sales',
  name: 'SalesFinanceShow',
  component: SalesFinanceShow
},
{
  path: '/admin/finance/income',
  name: 'IncomeFinanceShow',
  component: IncomeFinanceShow
},
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

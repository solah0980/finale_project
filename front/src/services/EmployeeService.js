import Api from '@/services/Api'

export default {
    views(){
        return Api().get("/employee/selectAll")
    },
    login(data){
        return Api().post("/employee/login",data)
    },
    create(data){
        return Api().post("/employee/create",data)
    },
    read(id){
        return Api().get("/employee/read/"+id)
    },
    update(data){
        return Api().put("/employee/edit/"+data.id,data)
    },
    delete(id){
        return Api().delete("/employee/delete/"+id)
    },
    upload(data){
        return Api().post("/employee/profile",data)
    },
    deleteProfile(data){
        return Api().delete("/employee/delete/image/"+data.profile)
    }
}
import Api from './Api';

export default {
    create(data){
        return Api().post('/product/create',data)
    },
    read(id){
        return Api().get('/product/read/'+id)
    },
    edit(data){
        return Api().put('/product/edit/'+ data.id,data)
    },
    delete(id){
        return Api().delete('/product/delete/'+id)
    },
    upload(data){
        return Api().post("/product/picture",data)
    },
    deletePicture(data){
        return Api().delete("/product/delete/image/"+data.picture)
    },
    views(){
        return Api().get('/product/selectAll')
    }
}
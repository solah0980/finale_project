import Api from './Api'

export default {
    create(data){
        return Api().post('/stock_product/create',data)
    },
    read(id){
        return Api().get('/stock_product/'+id)
    },
    views(id){
        return Api().get('/stock_product/shop/'+id)
    },
    edit(data){
        return Api().put('/stock_product/edit/'+data.id,data)
    },
    delete(id){
        return Api().delete('/stock_product/delete/'+id)
    },
    getBarcode(id){
        return Api().get('/stock_product/barcode/'+id)
    }
}
import Api from './Api'

export default {
    create(data){
        return Api().post('/type_product/create',data)
    },
    read(id){
        return Api().get('/type_product/read/'+id)
    },
    show(){
        return Api().get('/type_product/selectAll')
    },
    edit(data){
        return Api().put('/type_product/edit/'+data.id,data)
    },
    delete(id){
        return Api().delete('/type_product/delete/'+id)
    }
}
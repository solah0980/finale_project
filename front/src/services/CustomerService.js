import Api from './Api'

export default {
    create(data){
        return Api().post('/customer/create',data)
    },
    login(data){
        return Api().post('/customer/login',data)
    },
    read(id){
        return Api().get('/customer/'+id)
    },
    editPersonal(data){
        return Api().put('/customer/'+data.id,data)
    },
    changePassword(data){
        return Api().put('/customer/password/'+data.id,data)
    },
    shows(){
        return Api().get('/customer/selectAll')
    }
}
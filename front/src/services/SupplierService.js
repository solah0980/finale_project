import Api from './Api'

export default {
    create(data){
        return Api().post("/supplier/create",data)
    },
    read(id){
        return Api().get("/supplier/"+id)
    },
    edit(data){
        return Api().put("/supplier/edit/"+data.id,data)
    },
    delete(id){
        return Api().delete("/supplier/delete/"+id)
    },
    views(){
        return Api().get('/suppliers')
    }
}
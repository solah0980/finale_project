import Api from './Api'

export default {
    create(data){
        return Api().post('/shop_online',data)
    },
    getDataByCustomer(id){
        return Api().get('/shop_online/customer/'+id)
    },
    index(id){
        return Api().get('/shop_online/'+id)
    },
    cancelOrder(id){
        return Api().put('/shop_online/'+id)
    }
}
import Api from '@/services/Api'

export default {
    create(data){
        return Api().post('/store_branch/create',data)
    },
    views(){
        return Api().get('/store_branch/selectAll')
    },
    read(id){
        return Api().get('/store_branch/read/'+id)
    },
    edit(data){
        return Api().put('/store_branch/edit/'+data.id,data)
    },
    delete(data){
        return Api().delete('/store_branch/delete/'+data.id)
    },
    upload(data){
        return Api().post("/store_branch/picture",data)
    },
    deletePicture(data){
        return Api().delete("/store_branch/delete/image/"+data.picture)
    }
}
import Api from './Api'

export default {
    getSalesData(date){
        return Api().post("/finance/product_sales/"+date.id,{
            start_date:date.start_date,
            end_date:date.end_date,
        })
    },
    getIncomeData(date){
        return Api().post("/finance/income_statement/"+date.id,{
            start_date:date.start_date,
            end_date:date.end_date,
        })
    }
}
import Api from './Api'

export default {
    create(data){
        return Api().post("/order_product/create",data)
    },
    views(id){
        return Api().get("/order_products/"+id)
    },
    read(id){
        return Api().get("/order_product/"+id)
    },
    edit(data){
        return Api().put("/order_product/edit/"+data.id,data)
    },
    delete(id){
        return Api().delete("/order_product/delete/"+id)
    },
    updateStatusOrder(data){
        return Api().put("/order_product/update_status/"+data.id,data)
    },
    genPDF(data){
        return Api().post('/genpdf/order',data)
    }
}
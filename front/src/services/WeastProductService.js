import Api from './Api';

export default {
    create(data){
        return Api().post('/weast_product/create',data)
    },
    views(id){
        return Api().get('/weast_product/shop/'+id)
    },
    delete(id){
        return Api().delete('/weast_product/delete/'+id)
    }
}
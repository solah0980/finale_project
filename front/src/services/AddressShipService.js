import Api from './Api'

export default {
    create(data){
        return Api().post('/address_shipment/create',data)
    },
    delete(id){
        return Api().delete('/address_shipment/'+id)
    },
    getDataFromId(id){
        return Api().get('/address_shipment/'+id)
    }
}
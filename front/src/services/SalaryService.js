import Api from '@/services/Api'

export default {
    getEmployeeForEdit(id){
        return Api().get("/salary/edit/read/"+id)
    },
    getEmployee(data){
        return Api().post("/salary/employee/show",data)
    },
    createSalary(data){
        return Api().post("/salary/create",data)
    },
    getSalary(data){
        return Api().get("/salary/selectAll",{
            params:data
        })
    },
    editSalary(data){
        return Api().put("/salary/edit",data)
    },
    deleteSalary(data){
        return Api().delete("/salary/delete/"+data.idSalary)
    },
    EmployeeGetData(id){
        return Api().get('/salary/employee/'+id)
    }
}
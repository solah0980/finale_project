import Api from './Api'

export default {
    uploadPicture(data){
        return Api().post('/crm/upload',data)
    },

    sendMail(data){
        return Api().post('/crm/email',data)
    },

    createCampaign(data){
        return Api().post('/crm/promotion/create',data)
    },
    selectPromotionAll(){
        return Api().get('/crm/promotion')
    },
    getCode(data){
        return Api().post('/crm/get/code',data)
    },
    deletePromotion(id){
        return Api().delete('/crm/promotion/'+id)
    }
}
import Api from './Api'

export default {
    create(data){
        return Api().post('/pos/create',data)
    },
    index(id){
        return Api().get('/pos/'+id)
    },
    edit(data){
        return Api().put("/pos/"+data.data_sell.id,data)
    },
    delete(id){
        return Api().delete('/pos/'+id)
    },
    shows(data){
        return Api().post('/pos/shop/'+data.id_shop,data)
    }
}
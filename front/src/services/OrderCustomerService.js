import Api from './Api'

export default {
    getData(data){
        return Api().get('/shop_online_backend?status='+data.status+'&id='+data.id)
    },
    updateStatus(data){
        return Api().put('/shop_online_backend/'+data.id,data)
    },
    shows(data){
        return Api().post('/shop_online_backend/shop/'+data.id_shop,data)
    },
    delete(id){
        return Api().delete('/shop_online/'+id)
    }
    
}
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const {sequelize} = require('./models')
const config = require('./config/config')
const cors = require('cors')
const path = require('path')
const expressLayouts = require('express-ejs-layouts');

app.use(expressLayouts);
app.set('view engine', 'ejs');

app.use(bodyParser.json())
app.use(express.urlencoded({ extended: true }))
app.use(cors())
app.use("/assets", express.static(path.join(__dirname, 'public')))

require('./passport')
require('./routes')(app)


sequelize.sync().then(() =>{
    app.listen(config.port,()=>{
        console.log(`server listening on port ${config.port}`)
    })
}).catch((err) =>{
    console.log(err)
})
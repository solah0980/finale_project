const Employee = require('./controllers/EmployeeController')
const Salary = require('./controllers/SalaryController')
const Store_branch = require('./controllers/Store_branchController')
const TypeProduct =require('./controllers/TypeProductController')
const Product =require('./controllers/ProductController')
const StockProduct = require('./controllers/StockProductController')
const Weast_product = require('./controllers/WeastProductController')
const Order_product = require('./controllers/OrderProductController')
const Supplier = require('./controllers/SupplierController')
const Owner = require('./controllers/OwnerController')
const Customer = require('./controllers/CustomerController')
const Pos = require('./controllers/PosController')
const OnlineShop = require('./controllers/OnlineShopController')
const AddressShipment = require('./controllers/AddressShipController')
const Crm = require('./controllers/CrmController')
const Finance = require('./controllers/FinanceController')
const GenPDFOder = require('./controllers/GenPDF')
const loginAll = require('./controllers/loginAll')
const Authentication = require('./policies/isAuthen')
const multer = require('multer')
const fs = require('fs')
const path = require('path')

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'src/public/uploads')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})
var upload = multer({ storage: storage })
var deleteImage = (req,res)=>{
    try {
        fs.unlinkSync(path.join(__dirname,'public/uploads/'+req.params.name))
        res.send('delete success')
      } catch(err) {
        console.error(err)
      }
}
module.exports = (app)=>{
    // สร้าง PDF ORDER
    app.post("/genpdf/order",GenPDFOder.genPDF)

    //API ข้อมูลลูกค้า
    app.post('/customer/create',Customer.create)
    app.post('/customer/login',loginAll.customerLogin)
    app.get('/customer/selectAll',Customer.getAll)
    app.get('/customer/:id',Customer.index)
    app.put('/customer/:id',Customer.edit)
    app.put('/customer/password/:id',Customer.changePasswd)



    //API ข้อมูลพนักงาน
    app.get('/employee/selectAll',Employee.getAll)
    app.post('/employee/create',Employee.create)
    app.get('/employee/read/:id',Employee.index)
    app.put('/employee/edit/:id',Employee.edit)
    app.delete('/employee/delete/:id',Employee.delete)
    app.post('/employee/login',loginAll.employeeLogin)
    app.post('/employee/profile', upload.single('profile'),function (req, res) {
        res.send("upload success")
    })
    app.delete('/employee/delete/image/:name',deleteImage)
    

    //API ข้อมูลค่าจ้างพนักงาน
    app.get('/salary/edit/read/:id',Salary.getDataForEdit)
    app.post('/salary/employee/show',Salary.getEmployeeForPaySalary)
    app.post('/salary/create',Salary.create)
    app.get('/salary/selectAll',Salary.getAll)
    app.get('/salary/employee/:id',Salary.getDataFOrEmployee)
    app.put('/salary/edit',Salary.edit)
    app.delete('/salary/delete/:id',Salary.delete)

    //API ข้อมูลร้าน
    app.post('/store_branch/create',Store_branch.create)
    app.get('/store_branch/read/:id',Store_branch.index)
    app.put('/store_branch/edit/:id',Store_branch.edit)
    app.delete('/store_branch/delete/:id',Store_branch.delete)
    app.get('/store_branch/selectAll',Store_branch.getAll)
    app.post('/store_branch/picture', upload.single('picture'),function (req, res) {
        res.send("upload success")
    })
    app.delete('/Store_branch/delete/image/:name',deleteImage)

    //API ประเภทข้อมูล
    app.post('/type_product/create',TypeProduct.create)
    app.get('/type_product/read/:id',TypeProduct.index)
    app.get('/type_product/selectAll',TypeProduct.getAll)
    app.put('/type_product/edit/:id',TypeProduct.edit)
    app.delete('/type_product/delete/:id',TypeProduct.delete)

    //API สินค้า
    app.post('/product/create',Product.create)
    app.get('/product/read/:id',Product.index)
    app.get('/product/selectAll',Product.getAll)
    app.put('/product/edit/:id',Product.edit)
    app.delete('/product/delete/:id',Product.delete)
    app.post('/product/picture', upload.single('picture'),function (req, res) {
        res.send("upload success")
    })
    app.delete('/product/delete/image/:name',deleteImage)
    
    //API สต็อกสินค้า
    app.post('/stock_product/create',StockProduct.create)
    app.get('/stock_product/shop/:id',StockProduct.getStockForShop)
    app.get('/stock_product/barcode/:id',StockProduct.barcode)
    app.get('/stock_product/:id',StockProduct.index)
    app.put('/stock_product/edit/:id',StockProduct.edit)
    app.delete('/stock_product/delete/:id',StockProduct.delete)

    //API สินค้าเสีย
    app.post('/weast_product/create',Weast_product.create)
    app.get('/weast_product/shop/:id',Weast_product.getAll)
    app.delete('/weast_product/delete/:id',Weast_product.delete)
    
    //API สั่งซื้อสินค้า
    app.post('/order_product/create',Order_product.create)
    app.get('/order_products/:id',Order_product.getAll)
    app.get('/order_product/:id',Order_product.index)
    app.put('/order_product/update_status/:id',Order_product.updateStatusOrder)
    app.put('/order_product/edit/:id',Order_product.edit)
    app.delete('/order_product/delete/:id',Order_product.delete)

    //API สั่งซัพพลายเออร์
    app.post('/supplier/create',Supplier.create)
    app.get('/supplier/:id',Supplier.index)
    app.put('/supplier/edit/:id',Supplier.edit)
    app.delete('/supplier/delete/:id',Supplier.delete)
    app.get('/suppliers',Supplier.getAll)

    //API POS System
    app.get('/pos/:id',Pos.index)
    app.put('/pos/:id',Pos.edit)
    app.delete('/pos/:id',Pos.delete)
    app.post('/pos/shop/:id',Pos.getDataForShop)
    app.post('/pos/create',Pos.create)


    //API Online Shopping 
    app.post('/shop_online',OnlineShop.create)                                  /*  ลูกค้า    */
    app.get('/shop_online/customer/:id',OnlineShop.getDataForCustomer)          /*  ลูกค้า    */   
    app.get('/shop_online/:id',OnlineShop.index)                                /*  ลูกค้า    */
    app.delete('/shop_online/:id',OnlineShop.delete)                            /*  ลูกค้า    */
    app.put('/shop_online/:id',OnlineShop.cancelOrderByCustomer)                              /*  ลูกค้า    */
       
    app.post('/shop_online_backend/shop/:id',OnlineShop.getDataForShop)         /*  พนักงาน   */
    app.get('/shop_online_backend',OnlineShop.getDataForEmployee)               /*  พนักงาน   */
    app.put('/shop_online_backend/:id',OnlineShop.updateStatus)                 /*  พนักงาน   */

    //API ที่อยู่จัดส่ง
    app.post('/address_shipment/create',AddressShipment.create)
    app.get('/address_shipment/:id',AddressShipment.getDataFromId)
    app.delete('/address_shipment/:id',AddressShipment.delete)

    //API เจ้าของ
    app.post('/owner/login',loginAll.ownerLogin)
    app.post('/owner/register',Owner.register)

     //API CRM
    app.post('/crm/email',Crm.crm_email)
    app.post('/crm/upload', upload.single('crm'),function (req, res) {
        res.send("upload success")
    })
    app.post("/crm/promotion/create",Crm.createCampaign)
    app.get("/crm/promotion",Crm.getAllCampaign)
    app.post("/crm/get/code",Crm.findCampaign)
    app.delete("/crm/promotion/:id",Crm.deleteCampaign)

    //API การเงินและยอดขาย
    app.post("/finance/product_sales/:id",Finance.product_sales)
    app.post("/finance/income_statement/:id",Finance.income_statement)
}
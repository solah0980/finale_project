module.exports = (sequelize, DataTypes)=>{
    const Promotion = sequelize.define('Promotion', {
        name: DataTypes.STRING(10),
        code: DataTypes.STRING(50),
        price: DataTypes.INTEGER,
        price_buy: DataTypes.INTEGER,
        type: DataTypes.STRING(50),
        date_start: DataTypes.DATEONLY,
        date_stop: DataTypes.DATEONLY
    })

    Promotion.associate = function(models){
        Promotion.hasMany(models.Sell)
        Promotion.hasMany(models.HistoryToUsePromotion)
    }

    return Promotion
}
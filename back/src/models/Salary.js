module.exports = (sequelize, DataTypes)=>{
    const Salary = sequelize.define('Salary', {
        salary_total: DataTypes.INTEGER,
        date_salary: DataTypes.DATEONLY,
        note: DataTypes.STRING(50),
        status: DataTypes.STRING(50)
    })

    Salary.associate = function(models){
        Salary.belongsTo(models.Employee)
    }
    return Salary
}
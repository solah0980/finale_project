module.exports = (sequelize, DataTypes)=>{
    const Product = sequelize.define('Product', {
        name: DataTypes.STRING(50),
        description: DataTypes.TEXT,
        picture: DataTypes.STRING,
        cost: DataTypes.INTEGER,
        price: DataTypes.INTEGER
    })

    Product.associate = function(models){
        Product.hasMany(models.Detail_order_product)
        Product.belongsTo(models.Type_product)
        Product.belongsTo(models.Supplier)
    }

    return Product
}
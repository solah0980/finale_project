module.exports = (sequelize, DataTypes)=>{
    const Type_product = sequelize.define('Type_product', {
        name: DataTypes.STRING(50)
    })

    Type_product.associate = function(models){
        Type_product.hasMany(models.Product)
    }

    return Type_product
}
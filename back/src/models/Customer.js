const bcrypt = require('bcrypt')
module.exports = (sequelize, DataTypes) => {
    const Customer = sequelize.define('Customer', {
      name: DataTypes.STRING,
      lastname: DataTypes.STRING,
      sex: DataTypes.STRING(10),
      address: DataTypes.TEXT,
      tell: DataTypes.STRING(10),
      email: DataTypes.STRING,
      status: DataTypes.BOOLEAN,
      username: {
        type: DataTypes.STRING(50),
        unique: true,
      },
      password: DataTypes.STRING,
    })

    Customer.associate = function(models){
      Customer.hasMany(models.Sell)
      Customer.hasMany(models.Address_shipment)
      Customer.hasMany(models.HistoryToUsePromotion)
    }
    
    Customer.prototype.comparePassword = function (password) {
      return bcrypt.compareSync(password, this.password)
    }


    return Customer
  }
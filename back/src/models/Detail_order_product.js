module.exports = (sequelize, DataTypes)=>{
    const Detail_order_product = sequelize.define('Detail_order_product', {
        qty_order: DataTypes.INTEGER,
        qty_received: DataTypes.INTEGER,
        price: DataTypes.INTEGER,
    })

    Detail_order_product.associate = function(models){
        Detail_order_product.belongsTo(models.Product)
    }
    return Detail_order_product
}
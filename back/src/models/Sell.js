module.exports = (sequelize, DataTypes)=>{
    const Sell = sequelize.define('Sell', {
        date_sell: DataTypes.DATEONLY,
        type: DataTypes.STRING(50),
        status: DataTypes.STRING(50),
        cost_ship:DataTypes.INTEGER(),
    })

    Sell.associate = function(models){
        Sell.hasMany(models.Sell_detail)
        Sell.belongsTo(models.Address_shipment)
        Sell.belongsTo(models.Customer)
        Sell.belongsTo(models.Payment)
        Sell.belongsTo(models.Promotion)
        Sell.belongsTo(models.Employee)
        Sell.belongsTo(models.Owner)
        Sell.belongsTo(models.Store_branch)
    }

    return Sell
}
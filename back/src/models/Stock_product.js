module.exports = (sequelize, DataTypes)=>{
    const Stock_product = sequelize.define('Stock_product', {
        price: DataTypes.INTEGER,
        qty: DataTypes.INTEGER,
        date_exp: DataTypes.DATEONLY,
    })

    Stock_product.associate = function(models){
        Stock_product.hasMany(models.Sell_detail)
        Stock_product.hasMany(models.Weast_product)
        Stock_product.belongsTo(models.Detail_order_product)
        Stock_product.belongsTo(models.Store_branch)
    }

    return Stock_product
}
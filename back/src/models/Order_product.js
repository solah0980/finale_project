module.exports = (sequelize, DataTypes)=>{
    const Order_product = sequelize.define('Order_product', {
        status: DataTypes.STRING(50),
        date_order: DataTypes.DATEONLY
    })

    Order_product.associate = function(models){
        Order_product.hasMany(models.Detail_order_product,{as:"products"})
        Order_product.belongsTo(models.Employee)
        Order_product.belongsTo(models.Store_branch)
        Order_product.belongsTo(models.Supplier)
        Order_product.belongsTo(models.Owner)
    }

    return Order_product
}
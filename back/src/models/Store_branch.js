module.exports = (sequelize, DataTypes)=>{
    const Store_branch = sequelize.define('Store_branch',{
        no: DataTypes.INTEGER,
        address: DataTypes.TEXT,
        picture: DataTypes.STRING,
        tell: DataTypes.STRING(10),
        latitude: DataTypes.STRING(100),
        longitude: DataTypes.STRING(100),
    })

    Store_branch.associate = function(models){
        Store_branch.hasMany(models.Employee)
        Store_branch.hasMany(models.Sell)
        Store_branch.hasMany(models.Stock_product)
        Store_branch.hasMany(models.Order_product)
    }

    return Store_branch
}
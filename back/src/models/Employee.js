const bcrypt = require('bcrypt')
module.exports = (sequelize, DataTypes) => {
    const Employee = sequelize.define('Employee', {
      name: DataTypes.STRING(50),
      lastname: DataTypes.STRING(50),
      sex: DataTypes.STRING(50),
      tell: DataTypes.STRING(10),
      position: DataTypes.STRING(50),
      address: DataTypes.TEXT,
      username: {
        type: DataTypes.STRING(50),
        unique: true,
      },
      password: DataTypes.STRING,
      profile: DataTypes.STRING,
      salary: DataTypes.INTEGER,
    })

    Employee.associate = function(models){
      Employee.hasMany(models.Order_product)
      Employee.hasMany(models.Salary)
      Employee.hasMany(models.Sell)
      Employee.belongsTo(models.Store_branch)
    }

    Employee.prototype.comparePassword = function (password) {
      return bcrypt.compareSync(password, this.password)
    }
    
    return Employee
  }
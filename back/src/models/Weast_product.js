module.exports = (sequelize, DataTypes)=>{
    const Weast_product = sequelize.define('Weast_product', {
        date_weast_product: DataTypes.DATEONLY,
        detail: DataTypes.STRING(50),
        qty: DataTypes.INTEGER,
    })

    Weast_product.associate = function(models){
        Weast_product.belongsTo(models.Stock_product)
    }

    return Weast_product
}
module.exports = (sequelize, DataTypes)=>{
    const Address_shipment = sequelize.define('Address_shipment', {
        address: DataTypes.STRING(50),
        latitude: DataTypes.STRING(100),
        longitude: DataTypes.STRING(100)
    })

    Address_shipment.associate = function(models){
        Address_shipment.hasMany(models.Sell)
    }

    return Address_shipment
}
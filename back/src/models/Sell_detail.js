module.exports = (sequelize, DataTypes)=>{
    const Sell_detail = sequelize.define('Sell_detail', {
        qty: DataTypes.INTEGER,
        price: DataTypes.INTEGER,
    })
    
    Sell_detail.associate = function(models){
        Sell_detail.belongsTo(models.Stock_product)
    }

    return Sell_detail
}
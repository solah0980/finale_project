const bcrypt = require('bcrypt')
module.exports = (sequelize, DataTypes) => {
    const Owner = sequelize.define('Owner', {
      name: DataTypes.STRING,
      lastname: DataTypes.STRING,
      gender: DataTypes.STRING,
      address: DataTypes.TEXT,
      tell: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        unique: true
      },
      position: DataTypes.STRING,
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      isOwner: DataTypes.BOOLEAN,
    })

    Owner.associate = function(models){
      Owner.hasMany(models.Order_product)
      Owner.hasMany(models.Sell)
    }

    Owner.prototype.comparePassword = function (password) {
      return bcrypt.compareSync(password, this.password)
    }
    
    return Owner
  }
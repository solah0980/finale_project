module.exports = (sequelize, DataTypes)=>{
    const Payment = sequelize.define('Payment', {
        price: DataTypes.INTEGER,
        date_payment: DataTypes.DATEONLY,
        picture: DataTypes.STRING,
        status: DataTypes.STRING(50)
    })

    Payment.associate = function(models){
        Payment.hasOne(models.Sell)
    }

    return Payment
}
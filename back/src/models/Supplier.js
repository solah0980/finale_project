module.exports = (sequelize, DataTypes)=>{
    const Supplier = sequelize.define('Supplier', {
        name: DataTypes.STRING(),
        email: DataTypes.STRING(), 
        tell: DataTypes.STRING(10),
        address: DataTypes.TEXT
    })

    Supplier.associate = function(models){
        Supplier.hasMany(models.Order_product)
        Supplier.hasMany(models.Product)
    }

    return Supplier
}
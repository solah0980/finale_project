const {Type_product} = require('../models') 
module.exports = {
    async create(req, res) {
        try{
            let data = await Type_product.create(req.body)
            res.send(data)
        }catch(e){
            console.log(e.message)
        }
    },
    async index(req, res) {
        try{
            console.log(req.params.id)
            let id_type = req.params.id
            let data = await Type_product.findOne({
                where: {
                    id:id_type
                }
            })
            res.send(data)

        }catch(e){
            console.log(e.message)
        }
    },
    async edit(req, res) {
        try{
            let id_type = req.params.id
            let data = await Type_product.update(req.body,{
                where:{
                    id:id_type
                }
            })
            res.send(data)
        }catch(e){
            console.log(e.message)
        }
    },
    async delete(req, res) {
        try{
            let id_type = req.params.id
            let ans = await Type_product.destroy({
                where:{
                    id:id_type
                }
            }) 
            res.send("delete success")
        }catch(e){
            console.log(e.message)
        }
    },
    async getAll(req, res){
        try{
            let data = await Type_product.findAll()
            res.send(data)
        }catch(e){
            console.log(e)
        }
    }
}
const {Payment,Sell,Sell_detail,Stock_product,Address_shipment,Promotion,Detail_order_product,Product,Customer,Store_branch, 
  HistoryToUsePromotion}  = require('../models')
module.exports = {
    async create(req, res) {
        try{
            let {data,product,payment} = req.body
            let pay = await Payment.create(payment)
            data.PaymentId=pay.id
            let sell = await Sell.create(data)
            console.log("create sell success")
            product.forEach(async p=>{
                let data_product = {
                    qty:p.qty_buy,
                    price:p.price,
                    SellId:sell.id,
                    StockProductId:p.id
                }
                await Sell_detail.create(data_product)
                let data_stock = await Stock_product.findOne({
                    where:{
                        id:p.id
                    }
                })
                data_stock.qty-=p.qty_buy
                await Stock_product.update({qty:data_stock.qty},{
                    where:{
                        id:data_stock.id
                    }
                })
                console.log("update stock success")

            })
            if(data.PromotionId){
                let data_history = {
                    CustomerId: data.CustomerId,
                    PromotionId: data.PromotionId
                }
                await HistoryToUsePromotion.create(data_history)
            }
            res.send("success")
        }catch(e){
            console.log(e)
        }
    },
    async index(req, res){
        try{
            let id_order = req.params.id;
            let data = await Sell.findOne({
                include: [Address_shipment,Payment,Promotion,Customer,Store_branch,{
                    model:Sell_detail,
                    include:[{
                        model:Stock_product,
                        include: [{
                            model:Detail_order_product,
                            include: [Product]
                        }]
                    }]
                }],
                where:{
                    id:id_order
                }
            })
          
            data.Sell_details = data.Sell_details.map(p=>{
                return {
                    id:p.id,
                    qty:p.qty,
                    price:p.price,
                    SellId: p.SellId,
                    StockProductId:p.StockProductId,
                    product_name:p.Stock_product.Detail_order_product.Product.name,
                    product_picture:p.Stock_product.Detail_order_product.Product.picture,
                }
            })

            let ans = {
                detail:data,
                product:data.Sell_details
            }
            res.send(ans)
        }catch(e){
            console.log(e)
        }
    },
    async updateStatus(req, res){
        try{
            let text = req.body.status
            let id_order = req.params.id
            let data = await Sell.update({status:text},{
                where:{
                    id:id_order,
                }
            })
            if(text=="เตรียมส่ง" || text=="ยกเลิก"){
                let status_update = ""
                let data_sell = await Sell.findOne({
                    include: [Sell_detail],
                    where:{
                        id:id_order
                    }
                })
                if(text=="เตรียมส่ง"){
                     status_update = "สำเร็จ"
                }else{
                     status_update = "การชำระเงินไม่ถูกต้อง"
                     data_sell.Sell_details.forEach(async p=>{
                         let stock = await Stock_product.findOne({
                             where:{
                                 id:p.StockProductId
                             }
                         })
                         stock.qty+=p.qty
                         await Stock_product.update({qty: stock.qty},{
                             where:{
                                 id:stock.id
                             }
                         })
                     })
                     if(data_sell.Promotion){
                       let data = await HistoryToUsePromotion.destroy({
                         where:{
                           id:data_sell.Promotion.id
                         }
                       })
                     }
                }

                let ans = await Payment.update({status:status_update},{
                    where:{
                        id:data_sell.PaymentId,
                    }
                })
                /* console.log(data_sell) */
            }
            res.send(id_order)
        }catch(e){
            console.log(e)
        }
    },
    async delete(req,res){
        try{
          let id_sell = req.params.id
          let data = await Sell_detail.findAll({ 
            where:{
              SellId: id_sell,
            }
          })
          data.forEach(async p =>{
            let stock = await Stock_product.findOne({
              where:{
                id:p.StockProductId
              }
            })
            stock.qty+= p.qty;
            stock = await Stock_product.update({qty:stock.qty},{
              where:{
                id:stock.id
              }
            })
    
            let ans = await Sell_detail.destroy({
              where:{
                id:p.id
              }
            })
          })
    
          let sell_delete = await Sell.destroy({
            where:{
              id:id_sell,
            }
          })
    
          res.send("success")
        }catch (e) {
          console.log(e)
        }
      },
      async cancelOrderByCustomer(req, res){
        try{
          let id_order = req.params.id

          let data = await Sell.update({status:"ยกเลิก"},{
            where:{
                id:id_order,
              }
          })

          let data_sell = await Sell.findOne({
            include: [Sell_detail],
            where:{
                id:id_order
            }
        })

        data_sell.Sell_details.forEach(async p=>{
          let stock = await Stock_product.findOne({
              where:{
                  id:p.StockProductId
              }
          })
          stock.qty+=p.qty
          await Stock_product.update({qty: stock.qty},{
              where:{
                  id:stock.id
              }
          })

          res.status(200).json({
            status: 'succes',
            data: "สำเร็จ",
          })
      })
        }catch (e){
          console.log(e)
        }
      },
    async getDataForEmployee(req, res){
        try{
            let status_search = req.query.status
            let id_shop = req.query.id
            if(status_search.includes('check'))          status_search = "ตรวจสอบ"
            else if(status_search.includes('prepare'))   status_search = "เตรียมส่ง"
            else if(status_search.includes('delivery'))   status_search = "กำลังส่ง"
            else if(status_search.includes('success'))   status_search = "สำเร็จ" 
            let data = await Sell.findAll({
                include: [Address_shipment,Payment,Promotion,Customer,Store_branch,{
                    model:Sell_detail,
                    include:[{
                        model:Stock_product,
                        include: [{
                            model:Detail_order_product,
                            include: [Product]
                        }]
                    }]
                }],
                where:{
                    StoreBranchId:{
                        $eq:id_shop
                    },
                    status:{
                        $like:status_search
                    }
                }
            })
            data = data.map(p=>{
                return{
                    data:p,
                    product: p.Sell_details.map(p=>{
                        return {
                            id:p.id,
                            qty:p.qty,
                            price:p.price,
                            SellId: p.SellId,
                            StockProductId:p.StockProductId,
                            product_name:p.Stock_product.Detail_order_product.Product.name,
                            product_picture:p.Stock_product.Detail_order_product.Product.picture,
                        }
                    })
                }
            })
            res.send(data)
        }catch(e){
            console.log(e)
        }
    },
    async getDataForCustomer(req, res) {
        try{
            let id_customer = req.params.id
            let data = await Sell.findAll({ 
                include: [Sell_detail,Address_shipment,Payment,Promotion],
                where:{
                    CustomerId: {
                        $eq: id_customer
                    },
                },
                order:[['id','DESC']]
            })
            res.send(data)
        }catch(e){
            console.log(e)
        }
    },
    async getDataForShop(req, res) {
        try {
          let { start_date, end_date } = req.body;
          let id_shop = req.params.id;
          let data = await Sell.findAll({
            include: [
              Address_shipment,
              Customer,
              Payment,
              Promotion,
              {
                model: Sell_detail,
                include: [
                  {
                    model: Stock_product,
                    include: [
                      {
                        model: Detail_order_product,
                        include: [Product],
                      },
                    ],
                  },
                ],
              },
            ],
            where: {
              StoreBranchId: {
                $eq: id_shop,
              },
              type: {
                $eq: "online",
              },
              status:{
                $like:'สำเร็จ'
              },
              date_sell: {
                $between: [start_date, end_date],
              },
            },
            order: [["date_sell", "DESC"]],
          });
    
          data = data.map((p) => {
            return {
              id: p.id,
              cost_ship: p.cost_ship,
              date_sell: p.date_sell,
              type: p.type,
              status: p.status,
              AddressShipmentId: p.AddressShipmentId,
              CustomerId: p.CustomerId,
              PaymentId: p.PaymentId,
              PromotionId: p.PromotionId,
              StoreBranchId: p.StoreBranchId,
              Address_shipment: p.Address_shipment,
              Customer: p.Customer,
              Payment: p.Payment,
              Promotion: p.Promotion,
              product: p.Sell_details.map((p) => {
                return {
                  id: p.id,
                  qty_sell: p.qty,
                  price: p.price,
                  StockProductId: p.StockProductId,
                  name: p.Stock_product.Detail_order_product.Product.name,
                  product_id: p.Stock_product.Detail_order_product.Product.id,
                  product_description:
                    p.Stock_product.Detail_order_product.Product.description,
                  product_picture:
                    p.Stock_product.Detail_order_product.Product.picture,
                };
              }),
            };
          });
    
          res.send(data);
        } catch (e) {
          console.log(e);
        }
      },
}
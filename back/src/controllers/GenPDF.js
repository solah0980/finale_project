const fs = require("fs");
const pdf = require("pdf-creator-node");
const path = require("path");
const options = require("../helpers/option")

module.exports = {
  genPDF(req, res) {
    try {
      const { Employee, Supplier, products } = req.body;
      const html = fs.readFileSync(
        path.join(__dirname, "../template/index.html"),
        "utf-8"
      );

      const filename = Math.random() + "_doc" + ".pdf";

      let sumTotal = products.reduce((sumTotal, product) => sumTotal + product.Product.price * product.qty,0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      
      for(let i =0;i<products.length;i++) {
        products[i].no = i+1
        products[i].total = (products[i].qty * products[i].price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
      const obj = {
        order:{
          id:req.body.id,
          date:req.body.date_order,
        },
        employee:Employee,
        supplier: Supplier,
        shop:req.body.Store_branch,
        prodlist: products,
        sumTotal: sumTotal,
    }
     const document = {
        html: html,
        data: {
            order: obj
        },
        path: "./src/public/docs/" + filename
    }

    pdf.create(document, options)
            .then(x => {
                res.send(filename)
            }).catch(error => {
                console.log(error);
            });
  
            

    } catch (e) {
      console.log(e.message);
    }
  },
};

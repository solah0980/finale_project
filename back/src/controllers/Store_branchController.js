const { Store_branch, Employee, sequelize }  = require('../models')

module.exports = {
    async create(req,res){
        try{
            console.log(req.body)
            let store_branch = await Store_branch.create(req.body)
            res.send(store_branch)
        }catch(err){
            console.log(err.message)
        }
    },
    async index(req,res){
        try{
            let Store_branch_id = req.params.id
            let store_branch = await Store_branch.findOne({
                where:{
                    id: Store_branch_id
                }
            })
            if(!store_branch){
                return res.status(400).send({error:"not found shop data"})
            }
            res.send(store_branch)
        }catch(err){
            return res.status(400).send({error:"not found shop data"})
        }
    },
    async edit(req,res){
        try{
            let store_branch_id = req.params.id
            let store_branch = await Store_branch.update(req.body,{
                where:{
                    id: store_branch_id
                }
            })
            if(!store_branch){
                return res.status(400).send({error:"edit shop data error"})
            }
            res.send(req.body)
        }catch{
            return res.status(400).send({error:"edit shop data error"})
        }
    },
    async delete(req,res){
        try{
            let store_branch_id = req.params.id
            let store_branch = await Store_branch.destroy({
                where:{
                    id: store_branch_id
                }
            })
            if(!store_branch){
                return res.status(400).send({error:"delete shop data error"})
            }
            res.send("delete success")
        }catch{
            return res.status(400).send({error:"delete shop data error"})
        }
    },
    async getAll(req,res){
        try{
            let data = await Store_branch.findAll({
                include:[{
                    model:Employee,
                    as:"Employees",
                }],
                order:[['no','ASC']]
            })
            res.send(data)
        }catch(err){
            console.log(err.message)
        }
    }
}
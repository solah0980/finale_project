const {Product, Type_product,Supplier} = require('../models')

module.exports = {
    async create(req,res){
        try {
            let data = await Product.create(req.body)
            res.send(data)
        }catch(e){
            console.log(e)
        }
    },

    async index(req, res){
        try{
            let idProduct = req.params.id
            console.log(req.params)
            let data = await Product.findOne({
                include:[Type_product],
                where:{
                    id:idProduct
                }
            })
            res.send(data)
        }catch(e){
            console.log(e.message)
        }
    },
   async  edit(req, res) {
        try {
            let idProduct = req.params.id
            let data = await Product.update(req.body,{
                where:{
                    id:idProduct
                }
            })
            res.send(data)
        }catch(e){
            console.log(e)
        }
    },

    async delete(req, res) {
        try{
            let idProduct = req.params.id
            let data = await Product.destroy({
                where:{
                    id:idProduct
                }
            })
            res.send("delete success")
        }catch(e){
            console.log(e.message)
        }
    },

    async getAll(req, res) {
        try{
            let data = await Product.findAll({
                include:[Type_product,Supplier]
            })
            res.send(data)
        }catch(e){
            console.log(e.message)
        }
    }
}
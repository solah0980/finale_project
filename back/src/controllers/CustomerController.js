const {Customer} = require('../models')
const bcrypt = require('bcrypt');
const saltRounds = 10;
module.exports = {
    async create(req,res){
        try{
            let checkEmail = await Customer.findOne({
                where:{
                    email:{
                        $eq:req.body.email
                    }
                }
            })
            if(!!checkEmail){
                return res.send({err:"มีอีเมล์นี้ในระบบแล้ว"})
            }
            req.body.password = bcrypt.hashSync(req.body.password,saltRounds)
            let data = await Customer.create(req.body)
            res.send(data)
        }catch(e){
            res.send({err:"มี username นี้ในระบบแล้ว"})
        }
    },

    async index(req, res){
        try{
            let id_user = req.params.id
            let data = await Customer.findOne({
                where:{
                    id: id_user,
                }
            })
            res.send(data)
        }catch(e){
            console.log(e)
        }
    },

    async edit(req, res){
        try{
            /* console.log(req.body) */
            let id_user = req.params.id
            let data = await Customer.update(req.body,{
                where:{
                    id:id_user
                }
            })
            res.send(data)
        }catch(e){
            console.log(e)
        }
    },
    async changePasswd(req,res){
        try{
            let id_user = req.params.id
            let data = await Customer.findOne({
                where:{
                    id:id_user
                }
            })

            let chack_old_passwd = await data.comparePassword(req.body.old_password)

            if(chack_old_passwd==false){
                return res.send({error:"รหัสผ่านเดิมไม่ถูกต้อง"})
            }

            req.body.new_password = bcrypt.hashSync(req.body.new_password,saltRounds)

            console.log(req.body)

            let ans= await Customer.update({password:req.body.new_password},{
                where:{
                    id:id_user
                }
            })

            res.send("เปลี่ยนรหัสผ่านสำเร็จ")
        }catch(e){
            console.log(e)
        }
    },
    async getAll(req,res){
        try{
            let data = await Customer.findAll()
            res.send(data)
        }catch(e){
            console.log(e)
        }
    }
}
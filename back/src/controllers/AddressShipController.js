const {Address_shipment} = require('../models')
module.exports = {
    async create(req, res){
        try{
            let data = await Address_shipment.create(req.body);
            res.send("success")
        }catch(e){
            console.log(e)
        }
    },

    async delete(req,res){
        try{
            let id_data = req.params.id
            let data = await Address_shipment.destroy({
                where:{
                    id:id_data
                }
            })
            res.send("success")
        }catch(e){
            console.log(e)
        }
    },

    async getDataFromId(req,res){
        try{
            let id_user = req.params.id
            let data = await Address_shipment.findAll({
                where:{
                    CustomerId:{
                        $eq:id_user
                    }
                }
            })
            res.send(data)
        }catch(e){
            console.log(e)
        }
    }
}
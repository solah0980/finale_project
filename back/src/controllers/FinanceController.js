const e = require('express');
const {Sell,Sell_detail,Promotion,Order_product,Detail_order_product,Salary,Payment,Stock_product,Product,Employee} = require('../models')


module.exports ={
    async product_sales(req, res){
        try{
            let { start_date, end_date } = req.body;
            let id_shop = req.params.id;
            let data 
            if(id_shop=="all"){
                data = await Sell.findAll({
                    include: [
                    Payment,
                    Promotion,
                    {
                        model: Sell_detail,
                        include: [
                        {
                            model: Stock_product,
                            include: [
                            {
                                model: Detail_order_product,
                                include: [Product],
                            },
                            ],
                        },
                        ],
                    },
                    ],
                    where: {
                        status:{
                            $like:"สำเร็จ"
                        },
                        date_sell: {
                            $between: [start_date, end_date],
                        },
                    },
                    order: [["date_sell", "DESC"]],
                });

            }else{
                data = await Sell.findAll({
                    include: [
                    Payment,
                    Promotion,
                    {
                        model: Sell_detail,
                        include: [
                        {
                            model: Stock_product,
                            include: [
                            {
                                model: Detail_order_product,
                                include: [Product],
                            },
                            ],
                        },
                        ],
                    },
                    ],
                    where: {
                        status:{
                            $like:"สำเร็จ"
                        },
                        StoreBranchId: {
                            $eq: id_shop,
                        },
                        date_sell: {
                            $between: [start_date, end_date],
                        },
                    },
                    order: [["type", "DESC"]],
                });
            }
            

            data_ready = makeNewData(data)

            
            let info = {
                sales_type:CalProductTypeSalesSell(data_ready),
                sales_name:CalProductNameSalesSell(data_ready),
                sales_date:CalDateSalesSell(data_ready)
            }

            res.send(info)

        }catch(err){
            console.log(err)
        }
    },

    async income_statement(req, res) {
        try{
            let { start_date, end_date } = req.body;
            let id_shop = req.params.id;
            let sales
            let order
            let salary
            if(id_shop == 'all'){
                    sales = await Sell.findAll({
                    include: [
                    Payment,
                    Promotion,
                    {
                        model: Sell_detail,
                        include: [
                        {
                            model: Stock_product,
                            include: [
                            {
                                model: Detail_order_product,
                                include: [Product],
                            },
                            ],
                        },
                        ],
                    },
                    ],
                    where: {
                        status:{
                            $like:"สำเร็จ"
                        },
                        date_sell: {
                            $between: [start_date, end_date],
                        },
                    },
                    order: [["date_sell", "DESC"]],
                });

                    order = await Order_product.findAll({
                    include: [{
                        model:Detail_order_product,
                        as:'products'
                    }],
                    where: {
                        date_order: {
                            $between: [start_date, end_date],
                        },
                    },
                })

                   salary = await Salary.findAll({
                    where:{
                        date_salary: {
                            $between: [start_date, end_date],
                        },
                    }
                })
        }
        else{
                sales = await Sell.findAll({
                include: [
                Payment,
                Promotion,
                {
                    model: Sell_detail,
                    include: [
                    {
                        model: Stock_product,
                        include: [
                        {
                            model: Detail_order_product,
                            include: [Product],
                        },
                        ],
                    },
                    ],
                },
                ],
                where: {
                    status:{
                        $like:"สำเร็จ"
                    },
                    StoreBranchId: {
                        $eq: id_shop,
                    },
                    date_sell: {
                        $between: [start_date, end_date],
                    },
                },
                order: [["date_sell", "DESC"]],
            });

               order = await Order_product.findAll({
                include: [{
                    model:Detail_order_product,
                    as:'products'
                }],
                where: {
                    date_order: {
                        $between: [start_date, end_date],
                    },
                    StoreBranchId: {
                        $eq: id_shop,
                    },
                },
            })

                salary = await Salary.findAll({
                include:[{
                    model:Employee,
                    where: {
                        StoreBranchId: {
                            $eq: id_shop,
                        },
                    }
                }],
                where:{
                    date_salary: {
                        $between: [start_date, end_date],
                    },
                    
                }
            })
        }

            sales = makeNewData(sales)
            let data = {
                sales_income:CalSalesAndCostShipAndPromotion(sales),
                order_expenses:CalOrder(order),
                salary_expenses:CalSalary(salary)
            }
            res.send(data)
        }catch(err){
            console.log(err)
        }
    }
}

function makeNewData(data){              //จัดระเบียบข้อมูล
    return data.map((p) => {                   
        return {
        id: p.id,
        date_sell: p.date_sell,
        type: p.type,
        status: p.status,
        PaymentId: p.PaymentId,
        PromotionId: p.PromotionId,
        Payment: p.Payment,
        Promotion: p.Promotion,
        cost_ship: p.cost_ship,
        product: p.Sell_details.map((p) => {
            return {
            id: p.id,
            qty_sell: p.qty,
            price: p.price,
            StockProductId: p.StockProductId,
            name: p.Stock_product.Detail_order_product.Product.name,
            product_id: p.Stock_product.Detail_order_product.Product.id,
            product_description:
                p.Stock_product.Detail_order_product.Product.description,
            product_picture:
                p.Stock_product.Detail_order_product.Product.picture,
            };
        }),
        };
    });

}

function CalProductTypeSalesSell(data){              //คำนวนยอดขายแต่ละประเภท
    try{
        let d = []
        data.forEach(s=>{
            let index = d.findIndex(d=>d.type==s.type)
            if(index!=-1){
                d[index].qty+=s.product.reduce((a,b)=>a+b.qty_sell,0),
                /* d[index].price += parseInt(s.product.reduce((a,b)=>a+b.qty_sell*b.price,0))+parseInt((s.cost_ship?s.cost_ship:0))-parseInt((s.Promotion? s.Promotion.price:0)) */
                d[index].price += parseInt(s.product.reduce((a,b)=>a+b.qty_sell*b.price,0))
            }else{
                let data = {
                    type:s.type,
                    qty:s.product.reduce((a,b)=>a+b.qty_sell,0),
                   /*  price:parseInt(s.product.reduce((a,b)=>a+b.qty_sell*b.price,0))+parseInt((s.cost_ship?s.cost_ship:0))-parseInt((s.Promotion? s.Promotion.price:0)) */
                   price:parseInt(s.product.reduce((a,b)=>a+b.qty_sell*b.price,0))
                }
                d.push(data)
            }
            
        })
        return d
    }catch(err){    
        console.log(err)
    }
}

function CalProductNameSalesSell(data){                 //คำนวนยอดขายแต่ละยี่ห้อ
    try{
        let d = [];
        data.forEach(s=>{
            s.product.forEach(p=>{
                let index = d.findIndex(d=>d.product_id==p.product_id)
                if(index!=-1){
                    d[index].qty+=p.qty_sell
                    d[index].price+=p.qty_sell*p.price
                }else{
                    let data = {
                        picture:p.product_picture,
                        product_id:p.product_id,
                        name:p.name,
                        qty:p.qty_sell,
                        price:p.price*p.qty_sell
                    }
                    d.push(data)
                }
            })
            
        })
        return d
    }catch(err){
        console.log(err)
    }
}

function CalDateSalesSell(data){                        //คำนวนยอดขายแต่ละวัน
    try{
        let d = [];
        data.forEach(s=>{
            let index = d.findIndex(d=>d.date==s.date_sell)
            if(index!=-1){
                d[index].qty+=s.product.reduce((a,b)=>a+b.qty_sell,0),
                /* d[index].price += parseInt(s.product.reduce((a,b)=>a+b.qty_sell*b.price,0))+parseInt((s.cost_ship?s.cost_ship:0))-parseInt((s.Promotion? s.Promotion.price:0)) */
                d[index].price += parseInt(s.product.reduce((a,b)=>a+b.qty_sell*b.price,0))
            }else{
                let data = {
                    date:s.date_sell,
                    qty:s.product.reduce((a,b)=>a+b.qty_sell,0),
                   /*  price:parseInt(s.product.reduce((a,b)=>a+b.qty_sell*b.price,0))+parseInt((s.cost_ship?s.cost_ship:0))-parseInt((s.Promotion? s.Promotion.price:0)) */
                    price:parseInt(s.product.reduce((a,b)=>a+b.qty_sell*b.price,0))
                }
                d.push(data)
            }
            
        })
        return d
    }catch(err){
        console.log(err)
    }
}

function CalSalesAndCostShipAndPromotion(data){
    try{
        let income = 0
        let cost_ship = 0
        let promotion = 0
        data.forEach(s=>{
            income += parseInt(s.product.reduce((a,b)=>a+b.qty_sell*b.price,0))
            cost_ship += s.cost_ship? s.cost_ship:0
            promotion += s.Promotion? s.Promotion.price:0
        })

        return {income,cost_ship,promotion}
    }catch(err){
        console.log(err)
    }
}

function CalOrder(data){
    try{
        let expenses = 0 
        data.forEach(d=>{
            d.products.forEach(p=>{
                expenses += p.qty_received*p.price
            })
        })
        return expenses
    }catch(err){
        console.log(err)
    }
}

function CalSalary(data){
    try{
        let expenses = 0 
        data.forEach(d=>{
            expenses += d.salary_total
        })
        return expenses
    }catch(err){
        console.log(err)
    }
}

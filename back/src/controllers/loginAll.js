const {Employee,Store_branch} = require('../models')
const {Owner,Customer} = require('../models')
const jwt = require('jsonwebtoken')
const config = require('../config/config')

function jwtSignUser (user) {
  const ONE_WEEK = 60 * 60 * 24 * 7
  return jwt.sign(user, config.authentication.jwtSecret, {
    expiresIn: ONE_WEEK
  })
}

module.exports={
  //ฟังก์ชั่นล็อกอินลูกค้า
  async customerLogin(req,res){
    const {username, password} = req.body
    const user = await Customer.findOne({
      where: {
        username: username
      }
    })
    if (!user) {
      return res.status(403).send({
        error: 'The login information was incorrect'
      })
    }
    const isPasswordValid = user.comparePassword(password, password)
    if (!isPasswordValid) {
      return res.status(403).send({
        error: 'The login information was incorrect password'
      })
    }
    if(user.status==false){
      return res.send({error:"บัญชีถูกระงับ โปรติดต่อเจ้าหน้าที่"})
    }
    /* console.log(user) */
    const userJson = user.toJSON()
    res.send({
      user: userJson,
      token: jwtSignUser(userJson)
    })
},

  //ฟังก์ชั่นล็อกอินของพนักงาน
    async employeeLogin(req,res){
        const {username, password} = req.body
        const user = await Employee.findOne({
          include:[Store_branch],
          where: {
            username: username
          }
        })
        if (!user) {
          return res.status(403).send({
            error: 'The login information was incorrect'
          })
        }
        const isPasswordValid = user.comparePassword(password, password)
        if (!isPasswordValid) {
          return res.status(403).send({
            error: 'The login information was incorrect password'
          })
        }
  
        const userJson = user.toJSON()
        res.send({
          user: userJson,
          token: jwtSignUser(userJson)
        })
    },

    //ฟังก์ชั่นล็อกอินของเจ้าของ
    async ownerLogin(req,res){
      const {username, password} = req.body
      const user = await Owner.findOne({
        where: {
          username: username
        }
      })
      if (!user) {
        return res.status(403).send({
          error: 'The login information was incorrect'
        })
      }
      const isPasswordValid = user.comparePassword(password, password)
      if (!isPasswordValid) {
        return res.status(403).send({
          error: 'The login information was incorrect password'
        })
      }
      user.isOwner = true
      const userJson = user.toJSON()
      res.send({
        user: userJson,
        token: jwtSignUser(userJson)
      })
  }
}
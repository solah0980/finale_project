const bcrypt = require('bcrypt');
const { Employee, Store_branch, Salary } = require('../models')
const saltRounds = 10;
module.exports = {
    async create(req, res) {
        try {
            let hashPassword = bcrypt.hashSync(req.body.password, saltRounds);
            req.body.password = hashPassword
            let user = await Employee.create(req.body)
            if (!user) {
                res.status(400).send({ error: "this username has already" })
            }
            res.send(user)
        } catch {
            return res.status(400).send({error:"create employee error"})
        }

    },
    async index(req, res) {
        try{
            let employee_id = req.params.id
            console.log(`employee_id = ${employee_id}`)
            let user = await Employee.findOne({
                where:{
                    id: employee_id
                }
            })
            if(!user){
                return res.status(400).send({error:"not found user"})
            }
            res.send(user)
        }catch{
            return res.status(400).send({error:"search error"})
        }
    },
    async edit(req, res) {
        try{
            let employee_id = req.params.id
            let user = await Employee.update(req.body,{
                where:{
                    id:employee_id
            }})
            if(!user){
                return res.status(400).send({error:"update failed"})
            }
            res.send(req.body)
        }catch{
            return res.status(400).send({error:"edit employee error"})
        }
    },
    async delete(req, res) {
        try{
            let employee_id = req.params.id
            let user = await Employee.destroy({
                where:{
                    id: employee_id
                }
            })
            if(!user){
                return res.status(400).send({error:"delete employee error"})
            }
            res.send("delete employee success")
        }catch{

        }
    },
    async getAll(req, res) {
        let users = await Employee.findAll({
            include: [{
                model:Store_branch,
                attributes:['address','no']
            }]
        })
        res.send(users)
    }
}
const {Salary, Employee,Store_branch} = require('../models')
module.exports ={
    async getEmployeeForPaySalary(req, res) {
        try{
            let {idShop,thisDate} = req.body
            /* console.log(idShop+" and "+thisDate) */
            let salaryAll = await Salary.findAll({
                where:{
                    date_salary:thisDate
                },
                attributes: ['EmployeeId',]
            })
            console.log(salaryAll)
            if(salaryAll.length == 0){
                let employee = await Employee.findAll({
                    where: {
                        StoreBranchId:idShop
                    }
                })
                return res.send(employee)
            }else{
                let num = salaryAll.map((salary)=>{
                    return salary.EmployeeId
                })
                let employee = await Employee.findAll({
                    where: {
                        StoreBranchId:idShop
                    }
                })
                let filterEmployee =  employee.filter(function(e) {
                    return this.indexOf(e.id) < 0;
                  },num)
                res.send(filterEmployee)
            }
        }catch(err){
            console.log(err.message)
        }
    },
    async create(req, res){
        try{
            let data = await Salary.create(req.body)
            res.send(data)
        }catch(err){
            console.log(err.message)
        }
    },

    async getAll(req, res){
        try{
            console.log(req.query)
            if(req.query.ShopId){
                let data = await Salary.findAll({
                    include:[{
                        model: Employee,
                        attributes:['profile','name','lastname','sex','position'],
                        where:{
                            $or:[{'$Employee.StoreBranchId$':req.query.ShopId}],
                        },
                        include:[Store_branch]
                    }],
                    where:{
                        date_Salary:{
                            $between:[req.query.startDate, req.query.endDate] 
                        },
                 }
                })
                let data2 = data.filter(e=>e.Employee!=null)
                res.send(data2)
            }else{
            let data = await Salary.findAll({
                include:[{
                    model: Employee,
                    attributes:['profile','name','lastname','sex','position'],
                    include:[Store_branch]
                }],
                where:{
                    date_Salary:{
                        $between:[req.query.startDate, req.query.endDate] 
                    }
                }
                
            })
            let data2 = data.filter(e=>e.Employee!=null)
            res.send(data2)
            res.send(data)
        }
        }catch(err){
            console.log(err.message)
        }
    },
    async delete(req, res) {
        try{
            let idSalary = req.params.id
            console.log(idSalary)
            let data = await Salary.destroy({
                where:{
                    id:idSalary
                }
            })
            res.send('delete success')
           
        }catch(err){
            return res.status(404).send({message:"delte salary error"})
        }
    },

    async getDataForEdit(req, res) {
        try{
            console.log(req.params)
            let data = await Salary.findOne({
                include:[{
                    model:Employee,
                    attributes:['profile','name','lastname','sex','position'],
                }],
                where:{
                    id:req.params.id
                }}
                )
            res.send(data)
        }catch(err){
            console.log(err)
        }
    },

    async edit(req, res) {
        try{
            let {id,salary_total,note,status} = req.body
            let data = await Salary.update({salary_total,note,status},{
                where:{
                    id
                }
            })
            res.send("แก้ไขข้อมูลสำเร็จ")
        }catch(err){
            console.log(err.message)
        }
    },

    async getDataFOrEmployee(req, res){
        try{
            let id_employee = req.params.id
            let data = await Salary.findAll({
                where:{
                    EmployeeId:{
                        $eq:id_employee
                    }
                }
            })
            let data_send = data.map(d=>{
                return{
                    title:`รับค่าจ้างแล้ว ${d.salary_total} บาท `,
                    date:d.date_salary
                }
            })
            res.send(data_send)
        }catch(err){
            console.log(e)
        }
    }

}
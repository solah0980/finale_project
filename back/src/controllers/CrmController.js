const {Promotion,HistoryToUsePromotion} = require('../models')
const path = require('path')
const fs = require('fs')
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'solah0980@gmail.com', // your email
      pass: 'solah098030' // your email password
    }
  });

module.exports = {
    async crm_email(req, res){
        try{
          /*  console.log(req.body); */
            var mailOptions = {
                from: "solah0980@gmail.com", // sender address
                to: req.body.email, // senderlist of receivers
                subject: req.body.subject, // Subject line
                html: '<p style="text-align:center;"><img src="cid:pictureCrm" width="500"/></p>',
                attachments:[
                  {
                    filename: 'image.png',
                    path: path.join(__dirname,'../public/uploads/'+req.body.picture),
                    cid: 'pictureCrm'
                  }
                ]
              }
            
              transporter.sendMail(mailOptions, function(error, response){
                if(error){
                  console.log(error);
                  res.send('Failed');
                }else{
                  console.log("Message sent: " + response.message);
                  res.send('Worked');
                }
              });
        }catch(e){
            console.log(e)
        }
    },

    async createCampaign(req, res){
      try{
        let ans = await Promotion.create(req.body)
        res.send("success")
      }catch(e){
        console.log(e)
      }
    },
    async findCampaign(req, res){
      try{
        let data = await Promotion.findOne({
          where:{
            code:{
              $eq:req.body.code
            }
          }
        })
        
        if(data){
          let ans = await HistoryToUsePromotion.findOne({
            where:{
              CustomerId:{
                $eq:req.body.id
              },
              $and:{
                PromotionId:{
                  $eq:data.id
                }
              }
            }
          })
          if(ans) {
            res.send({error:"เคยใช้ส่วนลดนี้แล้ว"})
          }else{
            res.send(data)
          }
          
        }else{
          res.send({error:"ไม่เจอโค้ดส่วนลด"})
        }
        
      }catch(e){
        console.log(e)
      }
    },
    async deleteCampaign(req, res){
      try{
        let id_c = req.params.id;
        let ans = await Promotion.destroy({
          where:{
            id:id_c,
          }
        })
        res.send("success")
      }catch(e){
        console.log(e)
      }
    },
    async getAllCampaign(req, res){
      try{
        let data = await Promotion.findAll({
          order:[
            ['id', 'ASC'],
          ]
        })
        res.send(data)
      }catch(e){
        console.log(e)
      }
    }
}
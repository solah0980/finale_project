const bcrypt = require('bcrypt');
const { Owner } = require('../models')
const saltRounds = 10;
module.exports = {
    async register(req, res) {
        try {
            let hashPassword = bcrypt.hashSync(req.body.password, saltRounds);
            req.body.password = hashPassword
            let user = await Owner.create(req.body)
            if (!user) {
                res.status(400).send({ error: "this username has already" })
            }
            res.status(200).send(user)
        } catch {
            return res.status(400).send({error:"this email has already been registered"})
        }

    },

    async getOwner(req, res) {
        let users = await Owner.findAll()
        res.send(users)
    }
}
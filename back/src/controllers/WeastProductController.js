const {Weast_product, Stock_product,Product,Type_product,Detail_order_product} = require('../models')

module.exports ={
    async create(req, res){
        try{
            let stock = await Stock_product.findOne({
                where:{
                    id:req.body.StockProductId
                }
            })
            let new_qty = {
                qty:stock.qty - req.body.qty
            }
            let data = await Weast_product.create(req.body)
            let stock_product = await Stock_product.update(new_qty,{
                where:{
                    id:stock.id
                }
            })
            res.send(data)
        }catch(e){
            console.log(e.message)
        }
    },
    async delete(req, res){
        try{
            let id_weast_product = req.params.id
            let data = await Weast_product.findOne({
                where:{
                    id: id_weast_product,
                }
            })
            let stock = await Stock_product.findOne({
                where:{
                    id:data.StockProductId
                }
            })
            let new_qty = {
                qty: data.qty+stock.qty
            }
            stock = await Stock_product.update(new_qty,{
                where:{
                    id:data.StockProductId
                }
            })
            data = await Weast_product.destroy({
                where:{
                    id: data.id
                }
            })
            res.send("ลบสำเร็จ")
        }catch(e){
            console.log(e)
        }
    },
    async getAll(req, res){
        try{
            let id_shop = req.params.id
            let data = await Weast_product.findAll({
                include: [{ 
                    model: Stock_product,
                    where:{
                        StoreBranchId: id_shop,
                    },
                    include:[{
                        model:Detail_order_product,
                        include: [{
                            model:Product,
                            include:[Type_product]
                        }],

                    }],
                }],
                order:[['id','DESC']]
            })

            data = data.map(p=>{
                return{
                    id:p.id,
                    StockProductId:p.StockProductId,
                    qty:p.qty,
                    detail:p.detail,
                    date_weast_product:p.date_weast_product,
                    Product:{
                        picture: p.Stock_product.Detail_order_product.Product.picture,
                        name: p.Stock_product.Detail_order_product.Product.name,

                    }
                }
            })
            res.send(data)
        }catch(e){
            console.log(e)
        }
    }
}
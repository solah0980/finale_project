const {Stock_product,Product,Store_branch,Type_product,Weast_product,Order_product,Detail_order_product} = require('../models')
const { Op } = require("sequelize");
const bwipjs = require('bwip-js');

module.exports ={
    async create(req,res){
        try{
            let {status_order,stock_product,update_detail_product} = req.body
            let ans_order = await Order_product.update({status:status_order.status},{
                where:{
                    id:status_order.id,
                }
            })
            update_detail_product.forEach(async p => {
                let ans = await Detail_order_product.update({qty_received:p.qty_received},{
                    where:{
                        id:p.id
                    }
                })
            })

            stock_product.forEach(async p => {
                let ans = await Stock_product.create(p)
            })
            res.send("สำเร็จ")
        }catch(e){
            console.log(e.message)
        }
    },
    async index(req, res){
        try{
            let id_stock = req.params.id
            let data = await Stock_product.findAll({
                include: [{
                    model:Detail_order_product,
                    include:[{
                        model:Product,
                        include:[Type_product]
                    }]
                },Store_branch],
                where:{
                    id:id_stock
                }
            })

            data = data.map(p=>{
                return {
                    id:p.id,
                    price:p.price,
                    qty:p.qty,
                    date_exp:p.date_exp,
                    detail_order:p.Detail_order_product,
                    product:p.Detail_order_product.Product
                }
            })
            res.send(data)
        }catch(e){
            console.log(e.message)
        }
    },
    async edit(req, res){
        try{
            let {stock_product,detail_order} = req.body
            let id_stock = req.params.id
            let ans_stock = await Stock_product.update(stock_product,{
                where:{
                    id:id_stock
                }
            })

            let ans_detail_order = await Detail_order_product.update({qty_received:detail_order.qty_received},{
                where:{
                    id:detail_order.id
                }
            })
            res.send("success")
        }catch(e){
            console.log(e.message)
        }
    },
    async delete(req, res){
        try{
            let id_stock = req.params.id
            let data = await Stock_product.destroy({
                where:{
                    id:id_stock
                }
            })
            res.send("delete success")
        }catch(e){
            console.log(e.message)
        }
    },
    async getStockForShop(req, res){
        try{
            let id_shop = req.params.id
            let data = await Stock_product.findAll({
                include:[{
                    model:Detail_order_product,
                    include:[{
                        model:Product,
                        include:[Type_product]
                    }]
                },Store_branch],
                where:{
                    StoreBranchId:id_shop,
                    qty:{
                        $gt:0
                    }
                }
            })
            data = data.map(p=>{
                return{
                    StoreBranchId:p.StoreBranchId,
                    id:p.id,
                    price:p.price,
                    qty:p.qty,
                    date_exp:p.date_exp,
                    product:p.Detail_order_product.Product
                }
            })
            res.send(data)
        }catch(e){
            console.log(e.message)
        }
    },

    barcode(req, res){
        try{
            let data = req.params.id
            bwipjs.toBuffer({
                bcid:        'code128',       // Barcode type
                text:        data,    // Text to encode
                scale:       3,               // 3x scaling factor
                height:      10,             // Bar height, in millimeters
                width: 35,
                includetext: true,            // Show human-readable text
                textxalign:  'center',        // Always good to set this
            }, function (err, png) {
                if (err) {
                    // `err` may be a string or Error object
                } else {
                    let encodedBuffer = png.toString('base64');
                    res.send(encodedBuffer)
                    // `png` is a Buffer
                    // png.length           : PNG file length
                    // png.readUInt32BE(16) : PNG image width
                    // png.readUInt32BE(20) : PNG image height
                }
            });
        }catch(e){
            console.log(e.message)
        }
    }
}
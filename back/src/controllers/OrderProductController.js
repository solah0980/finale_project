const { Order_product, Detail_order_product,Employee,Store_branch,Product,Supplier,Owner } = require("../models");
const { edit } = require("./StockProductController");

module.exports = {
  async create(req, res) {
    try {
     /*  console.log(req.body); */
      let { data, products } = req.body;
      let order=[]
      console.log(data)
      products.forEach(product => {
          let index = order.findIndex(o => o.d.SupplierId === product.SupplierId);
          console.log(index);
          if(index !== -1) {
            order[index].p.push(product)
          }else{
            let data_order = {
              date_order:data.date_order,
              StoreBranchId: data.StoreBranchId,
              status: data.status,
              EmployeeId: data.EmployeeId,
              SupplierId:product.SupplierId,
              OwnerId:data.OwnerId,
            }
            order.push({
              d:data_order,
              p:[product]
            })
          }
          /* console.log(index) */
      })
      
      order.forEach(async o=>{
        let order = await Order_product.create(o.d);
        let id_order = JSON.stringify(order.id);
        o.p.forEach(async p=>{
          p.OrderProductId = id_order;
          await Detail_order_product.create(p);
        })
      })
      res.send("success")

     /*  let order = await Order_product.create(data);
      let id_order = JSON.stringify(order.id);

      products.forEach(async (product) => {
        product.OrderProductId = id_order;
        await Detail_order_product.create(product);
      });
      res.send(id_order); */

    } catch (e) {
      console.log(e.message);
    }
  },
  async index(req, res) {
    try {
      let id_order = req.params.id;
      let data = await Order_product.findOne({
        include: [
          {
            include:[Product],
            model: Detail_order_product,
            as: "products",
          },Store_branch,Employee,Supplier,Owner
        ],
        where: {
          id: id_order,
        },
      });
      res.send(data);
    } catch (e) {
      console.log(e.message);
    }
  },

  async edit(req, res) {
    try {
      let order_id = req.params.id;
      let {  products } = req.body;

      await Detail_order_product.destroy({
        where:{
          OrderProductId:{
            $eq: order_id
          }
        }
      })

      products.forEach(async (product) => {
        product.OrderProductId = order_id;
        await Detail_order_product.create(product);
      });


      let order = await Order_product.update(req.body,{
        where:{
          id:order_id
        }
      })

      res.send("แก้ไขสำเร็จ")
    } catch (e) {
      console.log(e.message);
    }
  },

  async delete(req, res) {
    try{
        let order_id = req.params.id
        console.log(order_id)
        let detail_order = await Detail_order_product.destroy({
          where:{
            OrderProductId:{
              $eq: order_id
            }
          }
        })
        let order = await Order_product.destroy({
          where:{
            id:order_id
          }
        })
        res.send("delete success")
    }catch (e) {
      console.log(e)
    }
  },
  async updateStatusOrder(req, res){
    try{
      let data = req.body
      let data_update={}
      if(data.note){
        data_update = {
          status:data.note,
        }
        let supplier = await Order_product.update(data_update,{
          where:{
            id:data.id
          }
        })
        res.send(supplier)
      }else{
        let data_update = {
        status:data.status,
        SupplierId:data.Supplier.id
      }
      let supplier = await Order_product.update(data_update,{
        where:{
          id:data.id
        }
      })
      res.send(supplier)
      }
      
    }catch (e) {
      console.log(e.message)
    }
  },
  async getAll(req, res) {
    try{
      let shop_id = req.params.id
      let data = await Order_product.findAll({
        include:[Employee,Owner,Supplier],
        where:{
          StoreBranchId:{
            $eq: shop_id,
          }
        },
        order:[['id','DESC']]
      });
      res.send(data)
    }catch (e) {
      console.log(e)
    }
  }
};

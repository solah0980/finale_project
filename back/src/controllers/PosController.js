const {
  Sell,
  Sell_detail,
  Stock_product,
  Detail_order_product,
  Product,
  Address_shipment,
  Customer,
  Payment,
  Promotion,
  Employee,
  Owner,
} = require("../models");
module.exports = {
  async create(req, res) {
    try {
      let { sell, sell_detail } = req.body;
      let ans_sell = await Sell.create(sell);

      sell_detail.forEach(async (p) => {
        let ans_sell_detail = await Sell_detail.create({
          SellId: ans_sell.id,
          StockProductId: p.StockProductId,
          price: p.price,
          qty: p.qty_sell,
        });

        let ans_stock_update = await Stock_product.update(
          { qty: p.qty_update },
          {
            where: {
              id: p.StockProductId,
            },
          }
        );
      });

      res.send("success");
    } catch (e) {
      console.log(e);
    }
  },
  async index(req, res) {
    try {
      let id_sell = req.params.id;
      let data = await Sell.findOne({
        include: [
          Employee,
          Owner,
          {
            model: Sell_detail,
            include: [
              {
                model: Stock_product,
                include: [
                  {
                    model: Detail_order_product,
                    include: [Product],
                  },
                ],
              },
            ],
          },
        ],
        where: {
          id: id_sell,
        },
      });

      data = {
        id: data.id,
        date_sell: data.date_sell,
        type: data.type,
        status: data.status,
        EmployeeId: data.EmployeeId,
        StoreBranchId: data.StoreBranchId,
        Address_shipment: data.Address_shipment,
        Employee: data.Employee,
        Owner: data.Owner,
        product: data.Sell_details.map((p) => {
          return {
            id: p.id,
            qty_sell: p.qty,
            new_qty: p.qty,
            price: p.price,
            StockProductId: p.StockProductId,
            name: p.Stock_product.Detail_order_product.Product.name,
            product_id: p.Stock_product.Detail_order_product.Product.id,
            product_description:
              p.Stock_product.Detail_order_product.Product.description,
            product_picture:
              p.Stock_product.Detail_order_product.Product.picture,
          };
        }),
      };

      res.send(data);
    } catch (e) {
      console.log(e);
    }
  },
  async edit(req, res) {
    try {
      let { data_sell, data_item_delete } = req.body;
      let sell_update = await Sell.update(
        { date_sell: data_sell.date_sell },
        {
          where: {
            id: data_sell.id,
          },
        }
      );
    
      if(data_sell.product!=null){
      data_sell.product.forEach(async (p) => {
        let ans_product = await Sell_detail.update(
          { qty: p.new_qty },
          {
            where: {
              id: p.id,
            },
          }
        );

        let stock = await Stock_product.findOne({
          where: {
            id: p.StockProductId,
          },
        });
        stock.qty += p.qty_sell;
        stock.qty -= p.new_qty;
        let ans_stock = await Stock_product.update(
          { qty: stock.qty },
          {
            where: {
              id: stock.id,
            },
          }
        );
      });
    }
      if (data_item_delete) {
        data_item_delete.forEach(async (p) => {
          let sell_detail = await Sell_detail.destroy({
            where: {
              id: p.id,
            },
          });

          let stock = await Stock_product.findOne({
            where: {
              id: p.StockProductId,
            },
          });

          stock.qty +=p.qty_sell;
          let ans = await Stock_product.update(
            { qty: stock.qty },
            {
              where: {
                id: stock.id,
              },
            }
          );
        });
      }

      res.send("success");
    } catch (e) {
      console.log(e);
    }
  },

  async delete(req,res){
    try{
      let id_sell = req.params.id
      let data = await Sell_detail.findAll({ 
        where:{
          SellId: id_sell,
        }
      })
      data.forEach(async p =>{
        let stock = await Stock_product.findOne({
          where:{
            id:p.StockProductId
          }
        })
        stock.qty+= p.qty;
        stock = await Stock_product.update({qty:stock.qty},{
          where:{
            id:stock.id
          }
        })

        let ans = await Sell_detail.destroy({
          where:{
            id:p.id
          }
        })
      })

      let sell_delete = await Sell.destroy({
        where:{
          id:id_sell,
        }
      })

      res.send("success")
    }catch (e) {
      console.log(e)
    }
  },
  async getDataForShop(req, res) {
    try {
      let { start_date, end_date } = req.body;
      let id_shop = req.params.id;
      let data = await Sell.findAll({
        include: [
          Address_shipment,
          Customer,
          Payment,
          Promotion,
          Employee,
          Owner,
          {
            model: Sell_detail,
            include: [
              {
                model: Stock_product,
                include: [
                  {
                    model: Detail_order_product,
                    include: [Product],
                  },
                ],
              },
            ],
          },
        ],
        where: {
          StoreBranchId: {
            $eq: id_shop,
          },
          type: {
            $eq: "pos",
          },
          date_sell: {
            $between: [start_date, end_date],
          },
        },
        order: [["date_sell", "DESC"]],
      });

      data = data.map((p) => {
        return {
          id: p.id,
          date_sell: p.date_sell,
          type: p.type,
          status: p.status,
          AddressShipmentId: p.AddressShipmentId,
          CustomerId: p.CustomerId,
          EmployeeId: p.EmployeeId,
          PaymentId: p.PaymentId,
          PromotionId: p.PromotionId,
          StoreBranchId: p.StoreBranchId,
          Address_shipment: p.Address_shipment,
          Employee: p.Employee,
          Owner: p.Owner,
          Customer: p.Customer,
          Payment: p.Payment,
          Promotion: p.Promotion,
          product: p.Sell_details.map((p) => {
            return {
              id: p.id,
              qty_sell: p.qty,
              price: p.price,
              StockProductId: p.StockProductId,
              name: p.Stock_product.Detail_order_product.Product.name,
              product_id: p.Stock_product.Detail_order_product.Product.id,
              product_description:
                p.Stock_product.Detail_order_product.Product.description,
              product_picture:
                p.Stock_product.Detail_order_product.Product.picture,
            };
          }),
        };
      });

      res.send(data);
    } catch (e) {
      console.log(e);
    }
  },
};

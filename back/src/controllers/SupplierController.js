const {Supplier} = require('../models')

module.exports = {
    async create(req, res){
        try{
            let data = await Supplier.create(req.body)
            res.send(data)
        }catch(e){
            console.log(e)
        }
    },
    async index(req, res){
        try{
            let id_supplier = req.params.id
            let data = await Supplier.findOne({
                where: {
                    id:id_supplier
                }
            })
            res.send(data)
        }catch(e){
            console.log(e)
        }
    },
    async edit(req, res){
        try{
            let id_supplier = req.params.id
            let data = await Supplier.update(req.body,{
                where:{
                    id:id_supplier
                }
            })
            res.send(data)
        }catch(e){
            console.log(e)
        }
    },
    async delete(req, res){
        try{    
            let id_supplier = req.params.id
            let data = await Supplier.destroy({
                where:{
                    id:id_supplier
                }
            })
            res.send("delete success")
        }catch(e){
            console.log(e)
        }
    },
    async getAll(req,res){
        try{
            let data = await Supplier.findAll()
            res.send(data)
        }catch(e){
            console.log(e)
        }
    }
}